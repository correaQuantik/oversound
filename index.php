<?php

    $pagina = "Home";
    require_once "inc/header.php";

?>

    <!-- hero-section-start -->
    <div class="hero-section" id="home">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="hero-content">
                        <h1 class="hc-header"><img src="img/feel.png" alt=""></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- hero-section-end -->

    <!-- top-banner-area-start -->
    <div class="top-banner-area">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 paddingTopBottom banner-sm">
                    <img src="img/banner/nordik.png">
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 paddingTopBottom banner-sm">
                    <img src="img/banner/over-speaker.png"> 
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 paddingTopBottom banner-sm">
                    <img src="img/banner/xpro.png">  
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 paddingTopBottom banner-sm">
                    <img src="img/banner/ovx.png"> 
                </div>
            </div>
        </div>

    </div>
    <!-- top-banner-area-end -->
    <!-- converter-area-start -->
    <div class="converter-area">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
                    <div class="converter-content">
                        <h2>Trabalhamos para que nossos produtos possam propagar sempre os mais verdadeiros sentimentos e emoções!</h2>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- converter-area-end -->


    <!-- latest-news-area-start -->
    <div class="latest-news-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="latest-news-head">
                        <p class="section-title-p">Não deixe a música acabar!</p>
                        <h2 class="sectiont-title">Conheça nossos <span class="sectiont-title-bold">produtos</span></h2>
                    </div>
                </div>
            </div>
            <div class="row product-sm-carousel">

                <a class="col-md-12 col-sm-12 col-xs-12" href="produto.php">
                    <div class="single-news clearfix">
                        <div class="sn-img">
                            <img src="img/product/OVS-8L-Lateral1.png" alt="">
                        </div>
                        <div class="sn-content">
                            <h4 class="sn-title">MID RANGE<br>
                                <span class="bold">OVS-8L</span></h4>
                            <p class="sn-text">
                                <b>TAMANHO:</b> 8 "<br>
                                <b>POTÊNCIA RMS:</b> 200 W<br>
                                <b>SENSIBILIDADE:</b> 100 dB<br>
                                <b>IMPEDÂNCIA:</b> 8/16 OHMS<br>
                                <b>DIÂMETRO DA BOBINA:</b> 2 "
                            </p>
                        </div>
                    </div>
                </a>

                <a class="col-md-12 col-sm-12 col-xs-12" href="produto.php">
                    <div class="single-news clearfix">
                        <div class="sn-img">
                            <img src="img/product/OVS-8L-Lateral1.png" alt="">
                        </div>
                        <div class="sn-content">
                            <h4 class="sn-title">MID RANGE<br>
                                <span class="bold">OVS-8L</span></h4>
                            <p class="sn-text">
                                <b>TAMANHO:</b> 8 "<br>
                                <b>POTÊNCIA RMS:</b> 200 W<br>
                                <b>SENSIBILIDADE:</b> 100 dB<br>
                                <b>IMPEDÂNCIA:</b> 8/16 OHMS<br>
                                <b>DIÂMETRO DA BOBINA:</b> 2 "
                            </p>
                        </div>
                    </div>
                </a>

                <a class="col-md-12 col-sm-12 col-xs-12" href="produto.php">
                    <div class="single-news clearfix">
                        <div class="sn-img">
                            <img src="img/product/OVS-8L-Lateral1.png" alt="">
                        </div>
                        <div class="sn-content">
                            <h4 class="sn-title">MID RANGE<br>
                                <span class="bold">OVS-8L</span></h4>
                            <p class="sn-text">
                                <b>TAMANHO:</b> 8 "<br>
                                <b>POTÊNCIA RMS:</b> 200 W<br>
                                <b>SENSIBILIDADE:</b> 100 dB<br>
                                <b>IMPEDÂNCIA:</b> 8/16 OHMS<br>
                                <b>DIÂMETRO DA BOBINA:</b> 2 "
                            </p>
                        </div>
                    </div>
                </a>

                <a class="col-md-12 col-sm-12 col-xs-12" href="produto.php">
                    <div class="single-news clearfix">
                        <div class="sn-img">
                            <img src="img/product/OVS-8L-Lateral1.png" alt="">
                        </div>
                        <div class="sn-content">
                            <h4 class="sn-title">MID RANGE<br>
                                <span class="bold">OVS-8L</span></h4>
                            <p class="sn-text">
                                <b>TAMANHO:</b> 8 "<br>
                                <b>POTÊNCIA RMS:</b> 200 W<br>
                                <b>SENSIBILIDADE:</b> 100 dB<br>
                                <b>IMPEDÂNCIA:</b> 8/16 OHMS<br>
                                <b>DIÂMETRO DA BOBINA:</b> 2 "
                            </p>
                        </div>
                    </div>
                </a>

                <a class="col-md-12 col-sm-12 col-xs-12" href="produto.php">
                    <div class="single-news clearfix">
                        <div class="sn-img">
                            <img src="img/product/OVS-8L-Lateral1.png" alt="">
                        </div>
                        <div class="sn-content">
                            <h4 class="sn-title">MID RANGE<br>
                                <span class="bold">OVS-8L</span></h4>
                            <p class="sn-text">
                                <b>TAMANHO:</b> 8 "<br>
                                <b>POTÊNCIA RMS:</b> 200 W<br>
                                <b>SENSIBILIDADE:</b> 100 dB<br>
                                <b>IMPEDÂNCIA:</b> 8/16 OHMS<br>
                                <b>DIÂMETRO DA BOBINA:</b> 2 "
                            </p>
                        </div>
                    </div>
                </a>


            </div>
        </div>
    </div>
    <!-- latest-news-area-end -->



    <!-- success-story-start -->
    <div class="success-story-area">
        <div class="container">
            <div class="row">
<!--                 <div class="col-xs-12">
                    <div class="success-story-head">
                        <h2 class="sectiont-title">Avaliações dos <span class="sectiont-title-bold">nossos clientes</span></h2>
                        <p class="section-title-p">Sempre presente em nossa caminhada!</p>
                    </div>
                </div> -->
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="single-success">
                        <div class="su-img">
                            <img src="img/success-story/client1.jpg" alt="">
                        </div>
                        <div class="su-content">
                            <h4>Oliver Cleveland</h4>
                            <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="single-success">
                        <div class="su-img">
                            <img src="img/success-story/client2.jpg" alt="">
                        </div>
                        <div class="su-content">
                            <h4>Johan Smith</h4>
                            <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="single-success">
                        <div class="su-img">
                            <img src="img/success-story/client3.jpg" alt="">
                        </div>
                        <div class="su-content">
                            <h4>Daven Lorner</h4>
                            <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- success-story-end -->


<?php

    require_once "inc/footer.php";

?>