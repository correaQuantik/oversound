<?php

    $pagina = "Empresa";
    include("inc/header.php");

?>

    <!-- hero-section-start -->
    <div class="hero-section-sobre" id="home">
        <div class="container">

            <div class="crumbs">
                <nav>
                    <ul class="crumb">
                        <li><a class="crumb-home" href="#"><i class="fa fa-home"></i></a></li>
                        <li><a href="home.php">Home<i class="fa fa-angle-right"></i></a></li>
                        <li><a class="active" href="sobre.php">Sobre Nós</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <!-- hero-section-end -->
    <!--location-section start-->
    <div class="location-section">
        <div class="container">
            <div class="location">
                <img src="img/gmap.png" alt="" class="gmap" />
                <div class="location-text">
                    <h1><span>OVERSOUND</span> ALTO-FALANTES</h1>
                    <p>A Oversound Speakers é uma indústria de alto-falantes, drivers e tweeters para uso profissional e automotivo. A empresa nasceu em 1992 quando dois dos sócios decidiram sair da empresa em que trabalhavam para se tornar empreendedores e abrir o próprio negocio. Começaram consertando alto falantes em garagem, em São Caetano do Sul – SP, passando para um galpão pequeno, posteriormente para um de médio porte e hoje está instalada em 7.000m2 de área construida e 14.000m2 de área total, com uma arrojada sede administrativa, localizados em Pindamonhangaba – SP, região estratégica escolhida por sua localização logística. Com seus 23 anos de vida, fornece seus produtos ao mercado brasileiro, Américas, Europa, Japão. Alto-falantes, drivers e tweeters de alta qualidade para projetos de sistemas sonoro como PAs, casas noturnas, igrejas, salões de festas, cinemas, grandes trios elétricos e trios automotivos, paredões, radiolas etc. Desde de Janeiro de 2009, a Oversound incorporou muitas novidades:</p>
                </div>
                <div class="g-pin"></div>
            </div>
            <div class="row">
                <div class="facts">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="facts-left">
                            <div class="row">
                                <h1 class="fact-title">Facts</h1>
                                <div class="fact-lbs">
                                    <h1>141'702 lbs</h1>
                                    <p>Fitness Club Client's Fat Lost to Date</p>
                                </div>
                                <div class="fact-lbs">
                                    <h1>141'702 lbs</h1>
                                    <p>Fitness Club Client's Muscle Gained to Date</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="facts-right">
                            <div class="row">
                                <img src="img/facts/1.jpg" alt="FactImage" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--location-section end-->
    <!--Client Feedback section start-->
    <div class="feedback-section fix bg-gray">
        <div class="container">
            <div class="owl-carousel" id="owl-demo1">
                <div class="feadback-item">
                    <div class="col-md-4 col-sm-4" col-md-4 col-xs-12>
                        <div class="client-image">
                            <img src="img/client/client_image.jpg" alt="" />
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <div class="client-feadback">
                            <p class="p-title">JONT HENRY - CEO B&C</p>
                            <h3>“ Tempor malesuada at id nisi. Etiam nec ia massa. Nullam <span>quis ultrices</span> felis. Bitur dum at diam id finibus. Quisque finibus laoreet nibh, at ultrieces risus"</h3>
                        </div>
                    </div>
                </div>
                <div class="feadback-item">
                    <div class="col-md-4 col-sm-4" col-md-4 col-xs-12>
                        <div class="client-image">
                            <img src="img/client/client_image.jpg" alt="" />
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <div class="client-feadback">
                            <p class="p-title">JONT HENRY - CEO B&C</p>
                            <h3>“ Tempor malesuada at id nisi. Etiam nec ia massa. Nullam <span>quis ultrices</span> felis. Bitur dum at diam id finibus. Quisque finibus laoreet nibh, at ultrieces risus"</h3>
                        </div>
                    </div>
                </div>
                <div class="feadback-item">
                    <div class="col-md-4 col-sm-4" col-md-4 col-xs-12>
                        <div class="client-image">
                            <img src="img/client/client_image.jpg" alt="" />
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <div class="client-feadback">
                            <p class="p-title">JONT HENRY - CEO B&C</p>
                            <h3>“ Tempor malesuada at id nisi. Etiam nec ia massa. Nullam <span>quis ultrices</span> felis. Bitur dum at diam id finibus. Quisque finibus laoreet nibh, at ultrieces risus"</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Client Feedback section end-->
    <!--Facilities section start-->
    <div class="facilities-section">
        <div class="container">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="facilities-left">
                    <div class="row">
                        <h1>Our Facilities</h1>
                        <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="row">
                    <div class="embded-video">
                        <img src="img/embded/video_thumbnail.jpg" alt="thumbnail" />
                        <a class="video" href="https://www.youtube.com/watch?v=qWy_aOlB45Y"><i class="fa fa-play"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Facilities section end-->
    <!--brand logo section start-->
    <div class="brand-logo-section">
        <div class="container">
            <div class="row">
                <div class="owl-carousel" id="owl-demo2">
                    <div class="brand-logo">
                        <img src="img/brand/1.jpg" alt="brand logo" />
                    </div>
                    <div class="brand-logo">
                        <img src="img/brand/2.jpg" alt="brand logo" />
                    </div>
                    <div class="brand-logo">
                        <img src="img/brand/3.jpg" alt="brand logo" />
                    </div>
                    <div class="brand-logo">
                        <img src="img/brand/4.jpg" alt="brand logo" />
                    </div>
                    <div class="brand-logo">
                        <img src="img/brand/5.jpg" alt="brand logo" />
                    </div>
                    <div class="brand-logo">
                        <img src="img/brand/6.jpg" alt="brand logo" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--brand logo section end-->
    <!--brand logo section start-->
    <div class="instagram-logo-section">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-sm-2 col-xs-4">
                    <div class="user-img">
                        <div class="row">
                            <img src="img/instagram/1.jpg" alt="brand logo" />
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-4">
                    <div class="user-img">
                        <div class="row">
                            <img src="img/instagram/2.jpg" alt="brand logo" />
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-4">
                    <div class="user-img">
                        <div class="row">
                            <img src="img/instagram/3.jpg" alt="brand logo" />
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-4">
                    <div class="user-img">
                        <div class="row">
                            <img src="img/instagram/4.jpg" alt="brand logo" />
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-4">
                    <div class="user-img">
                        <div class="row">
                            <img src="img/instagram/5.jpg" alt="brand logo" />
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-4">
                    <div class="user-img">
                        <div class="row">
                            <img src="img/instagram/6.jpg" alt="brand logo" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--brand logo section end-->


<?php
include("inc/footer.php");
?>
