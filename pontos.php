<?php

    $pagina = "Pontos de Venda";
    require_once "inc/header.php";

?>

    <!-- hero-section-start -->
    <div class="hero-section-sobre" id="home">
        <div class="container">

            <div class="crumbs">
                <nav>
                    <ul class="crumb">
                        <li><a class="crumb-home" href="#"><i class="fa fa-home"></i></a></li>
                        <li><a href="home.php">Home<i class="fa fa-angle-right"></i></a></li>
                        <li><a class="active" href="sobre.php">Pontos de venda</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <!-- hero-section-end -->

    <!-- latest-news-area-start -->
    <div class="latest-news-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="latest-news-head">
                        <h2 class="sectiont-title">Conheça nossos <span class="sectiont-title-bold">pontos de revenda</span></h2>
                    </div>
                </div>
            </div>
            <div class="row product-sm-carousel">

                <div class="col-md-12 col-sm-12 col-xs-12" data-revenda='{"name":"teste", "lat": -37.82,"lng": 144.97}'>
                    <div class="single-news clearfix">
                        <div class="sn-img">
                            <img src="img/oversound-logo.png" alt="">
                        </div>
                        <div class="sn-content">
                            <h4 class="sn-title">Loja<br>
                                <span class="bold">Nome</span></h4>
                            <p class="sn-text">
                                Descrição
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12" data-revenda='{"name":"teste1", "lat": -37.80,"lng": 144.99}'>
                    <div class="single-news clearfix">
                        <div class="sn-img">
                            <img src="img/oversound-logo.png" alt="">
                        </div>
                        <div class="sn-content">
                            <h4 class="sn-title">Loja<br>
                                <span class="bold">Nome</span></h4>
                            <p class="sn-text">
                                Descrição
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12" data-revenda='{"name":"teste2", "lat": -37.9,"lng": 144.96}'>
                    <div class="single-news clearfix">
                        <div class="sn-img">
                            <img src="img/oversound-logo.png" alt="">
                        </div>
                        <div class="sn-content">
                            <h4 class="sn-title">Loja<br>
                                <span class="bold">Nome</span></h4>
                            <p class="sn-text">
                                Descrição
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12" data-revenda='{"name":"teste3", "lat": -37.5,"lng": 144}'>
                    <div class="single-news clearfix">
                        <div class="sn-img">
                            <img src="img/oversound-logo.png" alt="">
                        </div>
                        <div class="sn-content">
                            <h4 class="sn-title">Loja<br>
                                <span class="bold">Nome</span></h4>
                            <p class="sn-text">
                                Descrição
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12" data-revenda='{"name":"teste4", "lat": -37.85,"lng": 144.92}'>
                    <div class="single-news clearfix">
                        <div class="sn-img">
                            <img src="img/oversound-logo.png" alt="">
                        </div>
                        <div class="sn-content">
                            <h4 class="sn-title">Loja<br>
                                <span class="bold">Nome</span></h4>
                            <p class="sn-text">
                                Descrição
                            </p>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <!-- latest-news-area-end -->

    <!--google map markup area start-->
    <div id="map"></div>
    <!-- google map activation start-->
    <script>
    var map;
    var bounds;
    var infowindow;
    var markersArray = {};

    function clearOverlays() {
        for (var i = 0; i < markersArray.length; i++) {
            markersArray[i].setMap(null);
        }
        markersArray.length = 0;
    }

    function initMap() { 
        bounds = new google.maps.LatLngBounds();
        infowindow = new google.maps.InfoWindow();    
        var locations = $("[data-revenda]");

        map = new google.maps.Map(document.getElementById("map"), {
            zoom: 9,
            center: {lat:1,lng:1}

        });

        for (i = 0; i < locations.length; i++) {  
          var location = $(locations[i]).data("revenda");

          var marker = new google.maps.Marker({
            position: new google.maps.LatLng(location["lat"], location["lng"]),
            map: map
          });

          markersArray[location["name"]] = marker;

          //extend the bounds to include each marker's position
          bounds.extend(marker.position);

          google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
              infowindow.open(map, marker);
            }
          })(marker, i));
        }

        //now fit the map to the newly inclusive bounds
        map.fitBounds(bounds);

        $("[data-revenda]").on("click",function() {
            let location = $(this).data("revenda");
            infowindow.setContent(location["name"]);
            infowindow.open(map,markersArray[location["name"]]);

            map.setCenter(markersArray[location["name"]].getPosition());
        });
    }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBO_5h890WNShs_YLGivCBfs2U89qXR-7Y&callback=initMap"></script>
    <!-- google map activation end -->
    <!--google map area end-->


<?php

    require_once "inc/footer.php";

?>