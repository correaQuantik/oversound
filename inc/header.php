<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <!--- Basic Page Needs  -->
    <meta charset="utf-8">
    <title>Oversound || <?php echo $pagina ?></title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Mobile Specific Meta  -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
    <!-- CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/meanmenu.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/typography.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/quantik.css">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="img/favicon.ico">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body data-spy="scroll" data-target="#scroll-menu" data-offset="100">
    <!-- preloader -->
    <div id="preloader"></div>
    <!-- header-start -->
    <header>
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="header-top-left">
                            <ul>
                                <li><a href="https://www.facebook.com/oversoundaltofalantes" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://plus.google.com/+oversoundaltofalantes" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="https://www.youtube.com/user/OversoundOficial" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
                                <li><a href="https://twitter.com/Oversound_BR" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="header-top-right">
                            <div class="single-htr">
                                <img src="img/br.png" alt="" width="24px">
                            </div>
                            
                            <div class="single-htr">
                                <img src="img/en.png" alt="" width="24px">
                            </div>

                            <div class="single-htr">
                                <img src="img/es.png" alt="" width="24px">
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="menu-area hidden-sm hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="logo">
                            <a href="index.html"><img src="img/oversound-logo.png" alt="" ></a>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="menu">
                            <nav>
                                <ul>
                                    <li><a href="index.php">HOME</a></li>
                                    <li><a href="sobre.php">EMPRESA</a></li>
                                    <li><a href="produtos.php">PRODUTOS</a></li>
                                    <li><a href="pontos.php">PONTOS DE VENDAS</a></li>
                                    <li><a href="contato.php">CONTATO</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="top-search">
                            <form action="#">
                                <span id="search_icon" class="search-icon"><i class="fa fa-search"></i></span>
                                <button class="search-text">PROCURAR</button>
                                <!-- <input id="search_box" class="search-box" type="text" required="" placeholder="Search Here"> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mobile-menu-area visible-xs visible-sm">
                            <a href="index.html" class="centerObjs"><img src="img/oversound-logo-branco.png" width="300px" alt="" ></a>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="mobile_menu">

                            <nav id="mobile_menu_active">
                                <ul>
                                    <li><a href="index.php">HOME</a></li>
                                    <li><a href="sobre.php">EMPRESA</a></li>
                                    <li><a href="produtos.php">PRODUTOS</a></li>
                                    <li><a href="pontos.php">PONTOS DE VENDAS</a></li>
                                    <li><a href="contato.php">CONTATO</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- header-end -->
    