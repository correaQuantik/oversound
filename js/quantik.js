$(".product-sm-carousel").owlCarousel({
   	items:4,
   	loop:true,
    autoplay:true,
    autoplayTimeout:4000,
    autoplayHoverPause:true,
   	nav:false
});