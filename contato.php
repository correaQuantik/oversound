<?php

    $pagina = "Contato";
    require_once "inc/header.php";

?>

    <!-- hero-section-start -->
    <div class="hero-section-sobre" id="home">
        <div class="container">

            <div class="crumbs">
                <nav>
                    <ul class="crumb">
                        <li><a class="crumb-home" href="#"><i class="fa fa-home"></i></a></li>
                        <li><a href="home.php">Home<i class="fa fa-angle-right"></i></a></li>
                        <li><a class="active" href="sobre.php">Contato</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <!-- hero-section-end -->

    <!--google map markup area start-->
    <div id="map"></div>
    <!-- google map activation start-->
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBO_5h890WNShs_YLGivCBfs2U89qXR-7Y&callback=initMap"></script>
    <script>
    function initMap() {

        var location = {
            lat: -37.813628,
            lng: 144.963058
        }; // Here is the Location lat, lng .

        var map = new google.maps.Map(document.getElementById("map"), {
            zoom: 9,
            scrollwheel: false,
            center: location,
        });
        var marker = new google.maps.Marker({
            position: map.getCenter(),
            icon: 'img/icon/maplock.png',
            map: map
        });
    }
    </script>
    <!-- google map activation end -->
    <!--google map area end-->
    <!--GF section start-->
    <div class="gf-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="get-touch">
                        <h1 class="gf-title">Entre em contato</h1>
                        <p class="gf-sub-title">Se está interessado em trabalhar para gente, entre em contato</p>
                        <p class="gap">Caso tenha alguma dúvida, sugestão e/ou critica, utilize este canal para falar com a gente. Precisa saber como e onde comprar? Consulte nossos vendedores, eles irão te auxiliar.</p>
                        <p class="gap">Endereço: <span cass="bld">Av. Félix Galvão Cruz Simões, 420 Pindamonhangaba - SP - Bairro Feital</span>
                            <br/>
                            <p>Telefone:<span class="bld">55 (12) 3637-3207</span></p>
                            <p>Email: <a href="mailto:hello@albedo.com">site@oversound.com.br</a></p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="feadback">
                        <h1>FeedBack</h1>
                        <div class="form">
                            <div class="cf-msg"></div>
                            <form action="mail.php" method="post" id="cf">
                                <div class="input">
                                    <input class="focus" type="text" placeholder="Seu nome" id="fname" name="fname">
                                </div>
                                <div class="input">
                                    <input class="focus" type="text" placeholder="Email" id="email" name="email">
                                </div>
                                <div class="input">
                                    <input class="focus" type="text" placeholder="Assunto" id="subject" name="subject">
                                </div>
                                <div class="textarea">
                                    <textarea class="contact-textarea focus" placeholder="Mensagem" id="msg" name="msg"></textarea>
                                </div>
                                <button class="cont-submit" id="submit" name="submit">Enviar agora</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php

    require_once "inc/footer.php";

?>
