<?php

    $pagina = "Produtos";
    require_once "inc/header.php";

?>

    <!-- hero-section-start -->
    <div class="hero-section-sobre" id="home">
        <div class="container">

            <div class="crumbs">
                <nav>
                    <ul class="crumb">
                        <li><a class="crumb-home" href="#"><i class="fa fa-home"></i></a></li>
                        <li><a href="home.php">Home<i class="fa fa-angle-right"></i></a></li>
                        <li><a class="active" href="sobre.php">Produtos</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <!-- hero-section-end -->

    <!-- latest-news-area-start -->
    <div class="latest-news-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="latest-news-head">
                        <p class="section-title-p">Não deixe a música acabar!</p>
                        <h2 class="sectiont-title">Conheça nossos <span class="sectiont-title-bold">produtos</span></h2>
                    </div>
                </div>
            </div>
            <div class="row">

                <?php for ($count = 0; $count < 10; $count++) { ?>

                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="single-news clearfix">
                        <div class="sn-img">
                            <img src="img/product/OVS-8L-Lateral1.png" alt="">
                        </div>
                        <div class="sn-content">
                            <h4 class="sn-title">MID RANGE<br>
                                <span class="bold">OVS-8L</span></h4>
                            <p class="sn-text">
                                <b>TAMANHO:</b> 8 "<br>
                                <b>POTÊNCIA RMS:</b> 200 W<br>
                                <b>SENSIBILIDADE:</b> 100 dB<br>
                                <b>IMPEDÂNCIA:</b> 8/16 OHMS<br>
                                <b>DIÂMETRO DA BOBINA:</b> 2 "
                            </p>
                        </div>
                    </div>
                </div>

                <?php } ?>

            </div>
        </div>
    </div>
    <!-- latest-news-area-end -->



    <!-- top-banner-area-start -->
    <div class="top-banner-area">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 paddingTopBottom banner-sm">
                    <img src="img/banner/nordik.png">
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 paddingTopBottom banner-sm">
                    <img src="img/banner/over-speaker.png"> 
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 paddingTopBottom banner-sm">
                    <img src="img/banner/xpro.png">  
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 paddingTopBottom banner-sm">
                    <img src="img/banner/ovx.png"> 
                </div>
            </div>
        </div>

    </div>
    <!-- top-banner-area-end -->
<?php

    require_once "inc/footer.php";

?>