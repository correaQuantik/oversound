<?php
    
    $pagina = "Produto";
    require_once "inc/header.php";

?>

    <!-- hero-section-start -->
    <div class="hero-section-sobre" id="home">
        <div class="container">

            <div class="crumbs">
                <nav>
                    <ul class="crumb">
                        <li><a class="crumb-home" href="#"><i class="fa fa-home"></i></a></li>
                        <li><a href="home.php">Home<i class="fa fa-angle-right"></i></a></li>
                        <li><a class="active" href="sobre.php">Produto</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <!-- hero-section-end -->

    <div class="single-pro-area">
        <div class="container">
            <div class="single-product">
                <div class="col-md-7 col-xs-12">
                    <div class="product-img-area">
                        <div class="product-img">
                            <div class="owl-carousel" id="demopro">
                                <div class="pro-single-img">
                                    <img src="img/product/OVS-8L-Lateral1.png" alt="product_image" />
                                </div>
                                <div class="pro-single-img">
                                    <img src="img/product/OVS-8L-Lateral1.png" alt="product_image" />
                                </div>
                                <div class="pro-single-img">
                                    <img src="img/product/OVS-8L-Lateral1.png" alt="product_image" />
                                </div>
                            </div>
                            <div class="r-top">
                                <p>8 pol.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-xs-12">
                    <div class="product-desc-area">
                        <h5 class="pro-cat">Machine</h5>
                        <h3 class="pro-title">OVS-8L</h3>
                        <p class="pro-desc">OVS -8L é um alto falante de elevada potência para sistemas profissionais em médias/altas e médias frequências que mantém resposta extremamente fiel com baixa distorção harmônica. Possui potente conjunto magnético de 200W RMS com bobina inside/outside de 2 polegadas em alumínio. O OVS-8L utiliza materiais de última geração que garantem qualidade, resistência e durabilidade a toda prova para todos os sistemas de som, em especial, para Line Array.</p>
                        <div class="tag-ar">
                            <table>
                                <tr>
                                    <td>
                                        <h5>tags:</h5></td>
                                    <td>
                                        <a href="#">Caixa para Instrumentos Musicais,</a>
                                        <a href="#">Caixa de Multi-Vias,</a>
                                        <a href="#">Coluna de Voz,</a>
                                        <a href="#">Monitor de Palco,</a>
                                        <a href="#">Trios/Mini-Trios,</a>
                                        <a href="#">Line Array,</a>
                                        <a href="#">Side Fill,</a>
                                        <a href="#">P.A,</a>
                                        <a href="#">Som pra Fora,</a>
                                        <a href="#">Igrejas/Templos,</a>
                                        <a href="#">Danceterias,</a>
                                        <a href="#">Cinemas,</a>
                                        <a href="#">Bandas,</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="share-ar">
                            <table>
                                <tr>
                                    <td>
                                        <h5>share:</h5></td>
                                    <td>
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                        <a href="#"><i class="fa fa-google-plus"></i></a>
                                        <a href="#"><i class="fa fa-pinterest"></i></a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="pro-description-area">
                    <nav>
                        <ul class="pro-desc-menu">
                            <li><a data-toggle="tab" href="#desc">Informações</a></li>
                            <li class="active" style="display: none"><a data-toggle="tab" href="#additional">Additional Information</a></li>
                        </ul>
                    </nav>
                    <div class="pro-review">
                        <div class="tab-content">
                            <div id="desc" class="tab-pane fade">
                                <h3 class="pro-title">ESPECIFICAÇÕES</h3>
                                <ul>
                                    <li><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>Impedância Nominal:    8/16 Ohms</li>
                                    <li><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>Resp. de Frequência:   150~6.000 Hz</li>
                                    <li><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>Freq. Mín. de Corte:   300 Hz</li>
                                    <li><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>Sensibilidade:     100 dB</li>
                                    <li><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>Potência RMS:  200 Watts</li>
                                    <li><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>Potência Musical:  400 Watts</li>
                                    <li><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>Diâmetro da Bobina:    2 Polegadas</li>
                                </ul>
                                <h3 class="pro-title">PARÂMETROS TS</h3>
                                <ul>
                                    <li><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>Freq. de Ressonância:  Fs 85 Hz</li>
                                    <li><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>Resist. da Bobina:     Re 6,5/11 Ω</li>
                                    <li><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>Fator de Qual. Total:  Qts 0.34</li>
                                    <li><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>Fator de Qual. Mecânica:   Qms 8</li>
                                    <li><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>Fator de Qual. Elétrica:   Qes 0.35</li>
                                    <li><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>Vol. Equiv. do Falante:    Vas 28 L</li>
                                    <li><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>Área Efetiva do Cone:  Sd 0.0288 m²</li>
                                    <li><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>Deslocamento Máximo:   Xmax 1 mm</li>
                                </ul>
                                <h3 class="pro-title">MATERIAIS</h3>
                                <ul>
                                    <li><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>Imã:   Ferrite de Bário</li>
                                    <li><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>Fio da Bobina:     CCAW®</li>
                                    <li><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>Corpo da Bobina:   Kapton®</li>
                                    <li><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>Carcaça:   Alumínio Injetado</li>
                                    <li><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>Cone:  Celulose</li>
                                    <li><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>Suspensão:     Tecido</li>
                                </ul>
                                <h3 class="pro-title">DIMENSÕES</h3>
                                <ul>
                                    <li><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>Diâmetro Maior:    218 mm</li>
                                    <li><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>Altura Total:  93.5 mm</li>
                                    <li><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>Perf. do Gabinete:     188 mm</li>
                                    <li><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>Diâmetro do Ímã:   155 mm</li>
                                    <li><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>Altura do Ímã:     17.5 mm</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--single product section end-->
    <!--related product section start-->
    <div class="related-pro">
        <div class="container">
            <h1 class="rel-pro-t">Related Products</h1>
            <div class="owl-carousel" id="related-pro">
                <?php for ($count = 0; $count < 10; $count++) { ?>
                <div class="rel-item">
                    <div class="product-item">
                        <div class="p-single-item">
                            <h1>OVS-8L</h1>
                            <img class="img1" src="img/product/OVS-8L-Lateral1.png" alt="product" />
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <!--related product section end-->

<?php

    require_once "inc/footer.php";

?>