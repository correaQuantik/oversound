/* Webkit */
::selection {
    background: #FF3300;
}
/* Gecko/Mozilla */
::-moz-selection {
    background: #FF3300;
}
.title .title_on_bottom_wrap .title_on_bottom_holder .title_on_bottom_holder_inner,
.q_progress_bar .progress_content,
.q_progress_bars_vertical .progress_content_outer .progress_content,
.qbutton,
.load_more a,
#submit_comment,
.drop_down .wide .second ul li .qbutton,
.drop_down .wide .second ul li ul li .qbutton,
.portfolio_gallery a .gallery_text_holder,
.filter_holder ul li.active span,
.filter_holder ul li:hover span,
.q_tabs .tabs-nav li.active a,
.q_tabs .tabs-nav li a:hover,
.q_accordion_holder.accordion .ui-accordion-header.ui-state-hover .accordion_mark,
.highlight,
.testimonials .testimonial_nav li.active a,
.gallery_holder ul li .gallery_hover,
.q_progress_bars_icons_inner.square .bar.active .bar_noactive,
.q_progress_bars_icons_inner.square .bar.active .bar_active,
.social_share_dropdown ul li.share_title,
.widget.widget_search form input[type="submit"]:hover,
.widget .tagcloud a,
.q_steps_holder .circle_small span,
.vc_text_separator.full div,
.mejs-controls .mejs-time-rail .mejs-time-current,
.mejs-controls .mejs-time-rail .mejs-time-handle,
.mejs-controls .mejs-horizontal-volume-slider .mejs-horizontal-volume-current,
.q_pie_graf_legend ul li .color_holder,
.q_line_graf_legend ul li .color_holder,
.circle_item .circle:hover,
.single_tags a,
.pagination ul li span,
.pagination ul li a:hover,
.portfolio_navigation .portfolio_prev a:hover,
.portfolio_navigation .portfolio_next a:hover,
.single_links_pages span,
.single_links_pages a:hover span,
.q_box_holder.with_icon,
.title .title_on_bottom_wrap.show_title_on_bottom .title_on_bottom_holder .title_on_bottom_holder_inner,
.shopping_cart_header .header_cart span,
.woocommerce div.message, 
.woocommerce .woocommerce-message, 
.woocommerce .woocommerce-error,
.woocommerce .woocommerce-info,
.woocommerce .button,
.woocommerce-page .button,
.woocommerce-page input[type="submit"],
.woocommerce input[type="submit"],
.woocommerce ul.products li.product .added_to_cart,
.woocommerce .product .onsale,
.woocommerce .product .single-onsale,
.woocommerce-pagination ul.page-numbers li span.current,
.woocommerce-pagination ul.page-numbers li a:hover,
.woocommerce .quantity .minus:hover, 
.woocommerce #content .quantity .minus:hover, 
.woocommerce-page .quantity .minus:hover, 
.woocommerce-page #content .quantity .minus:hover,
.woocommerce .quantity .plus:hover, 
.woocommerce #content .quantity .plus:hover, 
.woocommerce-page .quantity .plus:hover, 
.woocommerce-page #content .quantity .plus:hover,
.woocommerce .quantity input[type="button"]:hover, 
.woocommerce #content .quantity input[type="button"]:hover, 
.woocommerce-page .quantity input[type="button"]:hover, 
.woocommerce-page #content .quantity input[type="button"]:hover,
.woocommerce .quantity input[type="button"]:active, 
.woocommerce #content .quantity input[type="button"]:active, 
.woocommerce-page .quantity input[type="button"]:active,
.woocommerce-page #content .quantity input[type="button"]:active,
.woocommerce .widget_price_filter .price_slider_wrapper .ui-widget-content, 
.woocommerce-page .widget_price_filter .price_slider_wrapper .ui-widget-content,
.woocommerce .widget_price_filter .ui-slider .ui-slider-handle, 
.woocommerce-page .widget_price_filter .ui-slider .ui-slider-handle{
	background-color: #FF3300;
}


.portfolio_gallery a .gallery_text_holder,
.gallery_holder ul li .gallery_hover{
	background-color: rgba(255,51,0,0.9);
}

.q_icon_with_title.boxed .icon_holder .fa-stack:hover,
.q_social_icon_holder .fa-stack:hover{
	background-color: #FF3300 !important;
}

a:hover,
p a:hover,
.box_image_holder .box_icon .fa-stack i.fa-stack-base,
.q_icon_list i,
.q_progress_bars_vertical .progress_number,
.q_counter_holder span.counter,
.box_holder_icon .fa-stack i,
.q_percentage_with_icon,
.portfolio_like a.liked i,
.portfolio_like a:hover i,
.portfolio_single .portfolio_like a.liked i,
.portfolio_single .portfolio_like a:hover i,
.q_tabs.boxed .tabs-nav li.active a,
.q_tabs.boxed .tabs-nav li a:hover,
.q_tabs.vertical .tabs-nav li.active a,
.q_tabs.vertical .tabs-nav li a:hover,
.q_accordion_holder.accordion.with_icon .ui-accordion-header i,
.q_accordion_holder.accordion .ui-accordion-header:hover span.tab-title,
.testimonial_text_inner .testimonial_name .author_desc,
.q_message.with_icon > i,
.q_icon_with_title .icon_holder i,
.q_font_awsome_icon_square i,
.q_font_awsome_icon_stack i,
.q_icon_with_title .icon_with_title_link,
.q_font_awsome_icon i,
.q_progress_bars_icons_inner.normal .bar.active i,
.q_progress_bars_icons_inner .bar.active i.fa-circle,
.q_list.number.circle_number ul>li:before,
.q_list.number ul>li:before,
.blog_holder article .post_description a:hover,
.blog_holder article .post_description .post_author:hover,
.blog_holder article .post_description .post_comments:hover,
.blog_like a:hover i,
.blog_like a.liked i,
.blog_like a:hover span,
.social_share_dropdown ul li:hover .share_text,
.social_share_dropdown ul li :hover i,
#back_to_top:hover,
aside .widget #lang_sel ul ul a:hover,
aside .widget #lang_sel_click ul ul a:hover,
aside .widget #lang_sel_list li a.lang_sel_sel,
aside .widget #lang_sel_list li a:hover,
.portfolio_navigation .portfolio_button a:hover i,
.q_dropcap,
.woocommerce del,
.woocommerce-page del,
.woocommerce del .amount, .woocommerce-page del .amount,
.woocommerce .select2-results li.select2-highlighted,
.woocommerce-page .select2-results li.select2-highlighted,
.woocommerce-checkout .chosen-container .chosen-results li.active-result.highlighted,
.woocommerce-account .chosen-container .chosen-results li.active-result.highlighted,
.woocommerce div.product p[itemprop='price'] span.amount,
.woocommerce ul.tabs li a:hover,
.woocommerce ul.tabs li.active a,
.woocommerce div.cart-collaterals div.cart_totals table tr.order-total strong span.amount,
.woocommerce-page div.cart-collaterals div.cart_totals table tr.order-total strong span.amount,
.woocommerce div.cart-collaterals div.cart_totals table tr.order-total strong,
.woocommerce .checkout-opener-text a,
.woocommerce form.checkout table.shop_table tfoot tr.order-total th,
.woocommerce form.checkout table.shop_table tfoot tr.order-total td span.amount,
.woocommerce aside .widget ul.product-categories a:hover,
.woocommerce-page aside .widget ul.product-categories a:hover{
	color: #FF3300;
}

.q_icon_with_title.circle .icon_holder .fa-stack:hover i.fa-circle, 
.q_font_awsome_icon_stack:hover .fa-circle,
.footer_top a:hover{
	color: #FF3300 !important;
}

.ajax_loader_html,
.box_image_with_border:hover,
.q_progress_bars_icons_inner.square .bar.active .bar_noactive,
.q_progress_bars_icons_inner.square .bar.active .bar_active,
.portfolio_slider .flex-control-paging li a:hover,
.qode_carousels.gray .flex-control-paging li a:hover,
.testimonials .testimonial_nav li.active a,
.portfolio_slider .flex-control-paging li a.flex-active, 
.qode_carousels.gray .flex-control-paging li a.flex-active,
.widget #searchform.form_focus,
.q_call_to_action.with_border,
.woocommerce .widget_price_filter .ui-slider .ui-slider-handle, 
.woocommerce-page .widget_price_filter .ui-slider .ui-slider-handle{
	border-color: #FF3300;
}
span.highlight {
	background-color: #FF3300;
}




	
		.content{
			margin-top: -91px;
		}
	


.drop_down .second .inner ul,
.drop_down .second .inner ul li ul,
nav.main_menu > ul > li:hover > a span,
header.sticky nav.main_menu > ul > li:hover > a span,
.shopping_cart_header .header_cart:hover, 
.shopping_cart_header:hover .header_cart,
.shopping_cart_dropdown
{
	background-color: rgba(0,0,0,0.8);
}
nav.main_menu > ul > li > a{
	 color: #000000; 		 font-size: 17px; 		}

.side_menu_button a,
.side_menu_button a:hover,
.shopping_cart_header .header_cart i{
	color: #000000;
}

.drop_down .second .inner > ul > li > a,
.drop_down .second .inner > ul > li > h3,
.drop_down .wide .second .inner > ul > li > h3,
.drop_down .wide .second .inner > ul > li > a,
.drop_down .wide .second .inner > ul li.sub .flexslider ul li  h5 a,
.drop_down .wide .second .inner > ul li .flexslider ul li  h5 a,
.drop_down .wide .second .inner > ul li.sub .flexslider ul li  h5,
.drop_down .wide .second .inner > ul li .flexslider ul li  h5,
.shopping_cart_dropdown ul li a,
.shopping_cart_dropdown span.total,
.shopping_cart_dropdown span.total span{
	 color: #FFFFFF; 					}

.drop_down .second .inner > ul > li:hover > a,
.shopping_cart_dropdown ul li a:hover{
	color: #FF3300 !important;
}
.drop_down .wide .second .inner ul li.sub ul li a,
.drop_down .wide .second ul li ul li a,
.drop_down .second .inner ul li.sub ul li a,
.drop_down .wide .second ul li ul li a,
.drop_down .wide .second .inner ul li.sub .flexslider ul li .menu_recent_post,
.drop_down .wide .second .inner ul li .flexslider ul li .menu_recent_post a,
.drop_down .wide .second .inner ul li .flexslider ul li .menu_recent_post,
.drop_down .wide .second .inner ul li .flexslider ul li .menu_recent_post a{
	 color: #FFFFFF !important;  					}
.drop_down .wide.icons .second i{
	 color: #FFFFFF !important;  }
.drop_down .second .inner ul li.sub ul li:hover a,
.drop_down .second .inner ul li ul li:hover a,
.drop_down .wide.icons .second a:hover i
{
	color: #FF3300 !important;
}

header.sticky nav.main_menu > ul > li > a, 
header.light.sticky nav.main_menu > ul > li > a, 
header.dark.sticky nav.main_menu > ul > li > a{
	 color: #000000; 					}

 
header.sticky .side_menu_button a, header.sticky.light .side_menu_button a,
header.sticky .shopping_cart_header .header_cart i, header.sticky.light .shopping_cart_header .header_cart i{
	color: #000000; 
}


nav.mobile_menu ul li a,
nav.mobile_menu ul li h3{
	 color: #888888; 		font-family: Raleway, sans-serif;
					 font-weight: 900; 	}

@media only screen and (max-width: 1000px){
	header .side_menu_button a:hover,
	header .side_menu_button a,
	header .mobile_menu_button span,
	header .shopping_cart_header .header_cart i,
	header .shopping_cart_header .header_cart:hover i,
	header .shopping_cart_header:hover .header_cart i,
	header.dark .side_menu_button a,
	header.dark .side_menu_button a:hover,
	header.light .side_menu_button a,
	header.light .side_menu_button a:hover,
	header.dark .mobile_menu_button span,
	header.dark .shopping_cart_header .header_cart i,
	header.dark .shopping_cart_header .header_cart:hover i,
	header.dark .shopping_cart_header:hover .header_cart i{
		color: #888888;
	}
}
nav.mobile_menu ul li a:hover,
nav.mobile_menu ul li.active > a,
nav.mobile_menu ul li.current-menu-item > a{
	color: #000000;
}
	nav.mobile_menu ul li a,
	nav.mobile_menu ul li h3,
	nav.mobile_menu ul li ul li a,
	nav.mobile_menu ul li.open_sub > a:first-child{
		border-color: #999999;
	}

	@media only screen and (max-width: 1000px){
		.header_top{
			background-color: #CCCCCC !important;
		}
	}

	@media only screen and (max-width: 1000px){
		.header_bottom,
		nav.mobile_menu{
			background-color: #f6f6f6 !important;
		}
	}

h6{
	color: #ff3300; 						}

.q_icon_list p,
.q_list.circle ul>li,
.q_list.number ul>li{
	color: #ff3300; 			}		

	.qode_call_to_action.container{
		background-color:;
	}

