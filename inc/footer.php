

    <!--subscribe section start-->
    <div class="subscribe-section">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-5 col-xs-12">
                    <div class="row">
                        <div class="ht-newsletter">
                            <h5>Inscreva-se em</h5>
                            <h2>Nossa <b>newsletter</b></h2>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-sm-7 col-xs-12">
                    <div class="row">
                        <form class="form-inline newsletter-forn-in">
                            <div class="col-md-9  col-sm-9 col-xs-12">
                                <div class="form-group">
                                    <input class="form-control newsletter-input" type="text" placeholder="insira seu melhor e-mail">
                                </div>
                            </div>
                            <div class="col-md-3  col-sm-3 col-xs-12">
                                <div class="row text-center">
                                    <button type="submit" class="subscribe-btn">inscrever<i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--subscribe section end-->
    
    <footer>
        <div class="sticky-fotter">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6  col-xs-12">
                        <div class="sticky-fotter-text">
                            <p>OVERSOUND ALTO-FALANTES INDÚSTRIA E COMÉRCIO ELETRO ACÚSTICO<br>2017
                                © TODOS OS DIREITOS RESERVADOS.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6  col-xs-12">
                        <div class="fotter-link">
                            <nav>
                                <ul class="link-list">
                                    <li><a href="#">empresa</a></li>
                                    <li><a href="#">produtos</a></li>
                                    <li><a href="#">ponto de vendas</a></li>
                                    <li><a href="#">faq</a></li>
                                    <li><a href="#">contato</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <img src="img/bg/fotterbg.png" alt="" />
                    <a class="go-top" href="#home"><i class="fa fa-arrow-circle-o-up"></i></a>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer-end -->
    <!-- Scripts -->
    <script src="js/jquery-3.2.0.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/countdown.js"></script>
    <script src="js/jquery.meanmenu.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.scrollUp.js"></script>
    <script src="js/jquery.mixitup.min.js"></script>
    <script src="js/jquery.nstSlider.min.js"></script>
    <script src="js/handleCounter.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/theme.js"></script>
    <script src="js/quantik.js"></script>
</body>

</html>
